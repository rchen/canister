FROM ubuntu:14.04

# Install NodeJS via apt-get
RUN sudo apt-get install -y curl
RUN curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
RUN sudo apt-get install -y nodejs

# Test to make sure node is installed
CMD ["/usr/bin/node","-v"]
