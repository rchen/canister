var del = require('del');
var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var rs = require('run-sequence');

var srcProject = plugins.typescript.createProject('./src/tsconfig.json');


gulp.task('build-src', function() {
    return srcProject.src()
        .pipe(plugins.typescript(srcProject))
        .pipe(gulp.dest('./build'));
});

gulp.task('copy-knex', function() {
    return gulp.src('./src/knexfile.js')
        .pipe(gulp.dest('./build'));
});

gulp.task('copy-src-views', function() {
    return gulp.src('./src/**/*.hbs')
        .pipe(gulp.dest('./build'));
});

/**
 * Runs all of the build tasks defined
 */
gulp.task('build', ['build-src']);

/**
 * Cleans out the build directory
 */
gulp.task('clean', function() {
    return del('./build/**/*');
});

/**
 * Runs all of the copy tasks defined
 */
gulp.task('copy', ['copy-src-views', 'copy-knex']);

/**
 * Runs by default when only gulp is invoked
 */
gulp.task('default', function() {
    rs(
        'clean',
        ['build', 'copy'],
        'serve',
        'watch'
    );
});

/**
 * Starts up a nodemon server
 */
gulp.task('serve', function() {
    plugins.nodemon({
        ext: 'js hbs',
        script: './build/app.js'
    });
});

/**
 * Watch for changes
 */
gulp.task('watch', function() {
    gulp.watch('./src/**/*.ts', ['build-src']);
    gulp.watch('./src/**/*.hbs', ['copy-src-views']);
    gulp.watch('./src/knexfile.js', ['copy-knex']);
});
