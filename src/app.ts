import express = require('express');
import handlebars = require('express-handlebars');

let app = express();

// Register controllers
app.use(require('./controllers'));

// Use Handlebars as the templating engine for views
app.engine('hbs', handlebars.create({
    defaultLayout: 'default',
    extname: '.hbs',
    layoutsDir: __dirname + '/views/layouts'
}).engine);
app.set('view engine', 'hbs');
app.set('views', process.cwd() + '/build/views');

// Start the process at localhost:8080
app.listen(8080);
