import bodyParser = require('body-parser');
import express = require('express');

import Message = require('../interfaces/message');
import User = require('../models/user');

let urlParser = bodyParser.urlencoded({ extended: false });
let router = express.Router();

/**
 * Lists all users registered in the system
 * @return {User[]} users List of users
 */
router.get('/', (req, res) => {
    User.all((resp: User.User[]) => {
        res.status(200).json(resp);
    });
});

/**
 * Creates a new user
 * @param {string} email
 * @param {string} password
 * @return {Message} message
 */
router.post('/', urlParser, (req, res) => {
    let user = req.body as User.User;
    User.create(user, (resp: Message) => {
        res.json(200, resp);
    });
});

/**
 * Logs in a user
 * @param {string} email
 * @param {string} password
 */
 router.post('/login', urlParser, (req, res) => {
    let potentialUser = req.body as User.User;
    User.verify(potentialUser, (user: User.User) => {
        if (user) {
            res.json(200, user);
        } else {
            res.sendStatus(401);
        }
    });
 });

module.exports = router;
