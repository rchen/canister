import express = require('express');
import multer = require('multer');
import uuid = require('node-uuid');

import Image = require('../models/image');
import User = require('../models/user');

let router = express.Router();
let upload = multer({ dest: '/opt/canister' });

/**
 * Uploads an image to the system
 */
router.post('/', User.authorizeToken, upload.single('image'), (req, res) => {
    let user = req.authenticatedUser as User.User;
    let image = new Image.Image(req.file.path, user.id);
    Image.create(image, () => {
        res.status(200).json(image);
    });
});

/**
 * Gets an image from the system
 */
router.get('/:id', (req, res) => {
    Image.get(req.params["id"], (image: Image.Image) => {
        if (image) {
            res.header("Content-Type", "image/jpeg");
            res.sendFile(image.path);
        } else {
            res.sendStatus(404);
        }
    });
});

module.exports = router;
