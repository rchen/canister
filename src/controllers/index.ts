import express = require('express');

let router = express.Router();

router.use('/images', require('./images'));
router.use('/users', require('./users'));

router.get('/', (req, res) => {
    res.render('index');
});

module.exports = router;
