import uuid = require('node-uuid');

import db = require('../db');

export class Image {
    public static TABLE = 'images';

    public path: string;
    public uuid: string;
    public user_id: number;

    constructor(path: string, user_id: number) {
        this.path = path;
        this.user_id = user_id;
        this.uuid = uuid.v4();
    }
}

export function create(image: Image, cb: Function) {
    db(Image.TABLE).insert(image).then(() => {
        cb();
    });
}

export function get(uuid: string, cb: Function) {
    db(Image.TABLE).where({
        uuid: uuid
    }).first().then((existingImage: Image) => {
        cb(existingImage);
    });
}
