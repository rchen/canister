import bcrypt = require('bcrypt');
import db = require('../db');
import express = require('express');
import uuid = require('node-uuid');

export class User {
    public static TABLE = 'users';

    public id: number;
    public email: string;
    public password: string;
    public token: string;

    constructor(email: string, password: string) {
        this.email = email;
        this.password = password;
    }
}

/**
 * Selects all users from the database.
 */
export function all(cb: Function) {
    db(User.TABLE).select().then((users: User[]) => {
        cb(users);
    });
}

/**
 * Creates a new user.
 * Only creates the user if it does not exist in the database.
 */
export function create(user: User, cb: Function) {
    user.password = bcrypt.hashSync(user.password, 10);
    db(User.TABLE).where({
        email: user.email
    }).first().then((existingUser: User) => {
        if (existingUser) {
            cb({ message: 'Email is already registered' });
        } else {
            db(User.TABLE).insert(user).then(() => {
                cb({ message: 'Successfully registered' });
            });
        }
    });
}

/**
 * Verify a user in the database.
 */
export function verify(user: User, cb: Function) {
    db(User.TABLE).where({
        email: user.email
    }).first().then((existingUser: User) => {
        if (existingUser && bcrypt.compareSync(user.password, existingUser.password)) {
            existingUser.token = uuid.v4();
            db(User.TABLE).where({
                email: existingUser.email
            }).update({
                token: existingUser.token
            }).then(() => {
                delete existingUser.password;
                cb(existingUser);
            });
        } else {
            cb(null);
        }
    });
}

/**
 * Middleware for authorization of token.
 */
export function authorizeToken(req: express.Request, res: express.Response, next: Function) {
    let authorizationHeader = req.header("Authorization");
    if (!authorizationHeader) {
        res.sendStatus(401);
    } else {
        console.log(authorizationHeader);
        let bearerRegex = /Bearer ([\w-]+)/;
        db(User.TABLE).where({
            token: bearerRegex.exec(authorizationHeader)[1]
        }).first().then((existingUser: User) => {
            if (existingUser) {
                req.authenticatedUser = existingUser;
                next();
            } else {
                res.sendStatus(401);
            }
        });
    }
}
