import Knex = require('knex');

exports.up = (k: Knex, promise: Promise<any>) => {
    return k.schema.
        createTable('images', (t) => {
            t.integer('user_id');
            t.string('path');
            t.string('uuid');
        });
};

exports.down = (k: Knex, promise: Promise<any>) => {
    return k.schema.
        dropTable('images');
};
