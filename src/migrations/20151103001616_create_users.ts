import Knex = require('knex');

exports.up = (k: Knex, promise: Promise<any>) => {
    return k.schema.
        createTable('users', (t) => {
            t.increments();
            t.string('email');
            t.string('password');
            t.string('token');
        });
};

exports.down = (k: Knex, promise: Promise<any>) => {
    return k.schema.
        dropTable('users');
};
